local M = {}

M.general = {

  n = {
    -- One day I will figure how to use nvim-surround keymaps
    -- in here...
    ["<leader>iq"] = {'vES"', "Surround with \"\""},
  },

}

return M
