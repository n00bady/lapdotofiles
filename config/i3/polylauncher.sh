#!/usr/bin/env sh

# Terminate allready running instances
killall -q polybar

# Wait till the procs shut down
while pgrep -x polybar > dev/null; do sleep 1; done

# Launch polybar
polybar void &

