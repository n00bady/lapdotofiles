#!/bin/sh

# i3lock-color settings and colors

G='#00ff9fff'	# Light Greenish
C='#00b8ffff'	# Light Cyan
B='#001effff'	# Bright Blue
P='#bd00ffff'	# Purple not too deep
P1='#d600ffff'	# Bright Purple Pink
CL='#0080b222'	# Clearish
BCL='#001eff22' # Bright Blue again but with transparency
PCL='#bd00ff22' # Purple color again but clearish
WCL='#ffffff22'
F='Cantarell'
S='21'
BS='31'

/usr/bin/i3lock \
	--screen 1 \
	--blur 6 \
	--indicator \
	--clock \
	--timestr="%H:%M" \
	--timecolor=$G \
	--datestr="%A, %m %Y" \
	--datecolor=$G \
	--keylayou=2 \
	\
	--ringcolor=$G \
	--insidecolor=$CL \
        --verifcolor=$B \
	--separatorcolor=$B \
	--ring-width 4.0 \
	--linecolor=$CL \
	\
	--insidevercolor=$BCL \
	--ringvercolor=$C \
	\
	--insidewrongcolor=$PCL \
	--ringwrongcolor=$P1 \
	\
	--verifcolor=$G \
	--wrongcolor=$G \
       	--layoutcolor=$P1 \
	--keyhlcolor=$P1 \
	--bshlcolor=$P1 \
	\
	--time-font=$F \
	--date-font=$F \
	--layout-font=$F \
	--verif-font=$F \
	--wrong-font=$F \
	--timesize=$BS \
	--datesize=$S \
	--layoutsize=$S \
	--verifsize=$BS \
	--wrongsize=$BS \
	\
	--veriftext="Verifying..." \
	--wrongtext="WRONG!" \
	--noinputtext="Empty!"
