# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=2000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/jon/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Install starship for the theme, it's configuration is in ~/.config/starship.toml

# Install 
#         zsh-history-substring-searcch
#         zsh-syntax-highlighting
#         zsh-autosuggestions
# from the repos, manually installing them or not using them in with voidlinux
# probably will not work so make sure to replace the paths with the correct ones!
. /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
# ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#ff00ff,bg=cyan,bold,underline"
ZSH_AUTOSUGGEST_STRATEGY=(history completion)

eval "$(starship init zsh)"

# My own aliases
alias rot13="tr 'A-Za-z' 'N-ZA-Mn-za-m'"
alias papefetch="neofetch --backend w3m"
alias lah="exa -abghHliS --git"
alias pukeskull="sh /home/jon/Documents/gits/Color-Scripts/color-scripts/pukeskull"
alias dAt="sh /home/jon/Documents/random_scripts/drawAterminal.sh"
alias lsdir="exa -1D --icons | lolcat"
alias hexedit="hexedit --color"
alias oggart="/home/jon/Documents/random_scripts/ogg-cover-art.sh"
alias ytdl="/home/jon/Documents/random_scripts/ytdl.sh"
# alias kiff="kitty +kitten diff"
alias cd1="cd /mnt/d6f352a2-5fc3-42ed-8bef-e465e7933828/"
alias cd2="cd /mnt/0056EF2156EF15E6/"
alias cdjc="cd /mnt/d6f352a2-5fc3-42ed-8bef-e465e7933828/jc141/"
alias ls="exa --icons"
alias vim="nvim"
# alias hist10="history 0 | awk 'BEGIN {FS="[ \t]+|\\|"} {print $3}' | sort | uniq -c | sort -nr | head -n 10"
alias xsu="sudo xbps-install -Su"
alias xrs="xbps-query -Rs"
alias xin="sudo xbps-install -S"
alias wttr="curl -s wttr.in/thessaloniki | sed -n 1,7p && echo ------------------------------"
alias zoot="sudo -s zsh"
