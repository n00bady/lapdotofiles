# Startship
eval "$(starship init bash)"

# Completetions for sudo and man
complete -cf sudo
complete -cf man

# Enable checkwinsize so that bash will check the terminal size when
# it regains control
shopt -s checkwinsize
shopt -s expand_aliases
# Enable history appending instead of overwriting.
shopt -s histappend


# More Paths
export PATH=$PATH:/home/jon/.local/bin
export PATH=$PATH:/home/jon/.cargo/bin

# Aliases
#alias ls='ls --color=auto'
alias grep='grep --colour=auto'
alias egrep='egrep --colour=auto'
alias fgrep='fgrep --colour=auto'
alias cp='cp -i'
alias df='df -h'
alias free='free -m'
alias more=less
alias rot13="tr 'A-Za-z' 'N-ZA-Mn-za-m'"
alias papefetch='neofetch --backend w3m'
#alias lah='ls -ahl'
alias lah='exa -abghHliS --git'
#alias lsdir='ls -1p | grep / | lolcat'
alias lsdir='exa -1D --icons | lolcat'
alias hexedit='hexedit --color'
alias kiff='kitty +kitten diff'
alias wttr='curl -s wttr.in/thessaloniki | sed -n 1,7p && echo -----------------------'
alias xsu='sudo xbps-install -Su'
alias vim='nvim'
alias ls='exa --icons'

# For kitty terminal completetion
source <(kitty + complete setup bash)

# pywal colors
(cat ~/.cache/wal/sequences &)
source ~/.cache/wal/colors-tty.sh
alias reset='reset; (cat ~/.cache/wal/sequences &)'

. "$HOME/.cargo/env"

# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION

source ~/.dbus/session-bus/6aa24a8a9a38f3a568f2d9e35be7567e-0
