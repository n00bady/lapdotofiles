" ## Void-laptop .vimrc ##

" Vundle stuff
" ---------------------------------------------------
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

Plugin 'VundleVim/Vundle.vim'   " let Vundle manage Vundle, required!
Plugin 'davidhalter/jedi-vim'   " jedi-vim autocoplete for python plugin
"Bundle 'vim-ruby/vim-ruby'     " I might need it later...
Plugin 'preservim/nerdtree'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'liuchengxu/space-vim-dark'
Plugin 'tsiemens/vim-aftercolors'

" ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to
"                     auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
" ----------------------------------------------------



" Choose your colorscheme 
colorscheme space-vim-dark

"-------------------Note to self-----------------------------------------------
" Put these in ~/.vim/after/colors/common.vim with the vim-aftercolors plugin 
" installed to work, for some reason don't work if you only put them after the
" colorscheme in .vimrc
" For for italics on comments
" And transparrent background
"hi Comment cterm=italic
"hi Normal     ctermbg=NONE guibg=NONE
"hi LineNr     ctermbg=NONE guibg=NONE
"hi SignColumn ctermbg=NONE guibg=NONE
"------------------------------------------------------------------------------

set cursorline          " #Shows on which line you are :)
set number		" #To show line numbers
set textwidth=120	" #Line wrap
set linebreak		" #To breaklines at word
set showbreak=+++++	" #Prefix for the warp-broken line

set showmatch		" #To highlight the matching brace
set hlsearch		" #To highlight all the search results
set term=xterm
syntax on               " #Syntax highlight

set smartcase		" #Enable smartcase search
set ignorecase		" #Be always case sensitive
set incsearch		" #To search for strings incrementally

set autoindent		" #Auto-indent new lines
set expandtab		" #Use spaces instead of tabs
set softtabstop=4	" #Number of space per tab
set shiftwidth=4	" #Number of auto-indent spaces
set smartindent		" #Enable smart indent
set smarttab		" #Enable smart tabs

set ruler		" #Show row & column info
set undolevels=1000	" #Number of untos

set backspace=indent,eol,start	" #Backspace behaviour

set omnifunc=syntaxcomplete#Complete

"airline stuff ----------------------------------
let g:airline_powerline_fonts = 1
let g:airline_theme = 'deus'
"airline stuff end ------------------------------

" NERDTree Stuff --------------------------------
"nnoremap <Leader>f :NERDTreeToggle<Enter>
map <C-n> :NERDTreeToggle<CR>
"let g:NERDTreeDirArrowExpandable = '▸'
"let g:NERTreeDirArrowCollapsible = '▾'
let NERDTreeQuitOnOpen = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
" NERDTree stuff end ----------------------------
